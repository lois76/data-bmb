pa=[0.198
0.083
0.843
0.132
150
-29.77
-65.64
-2
-25.92
-90
-2.26
9.36
-1
-20.88
18.93
0.117
10.57
0.0001
0.001
0.76
0.001
0.039]

gCa=pa(1); gKir=pa(2); gK=pa(3); gL=pa(4);
ECa=pa(5); EK=pa(6); EL=pa(7);
V12mCa=pa(8); V12hCa=pa(9); V12hKir=pa(10); V12mK=pa(11); 
kmCa=pa(12); khCa=pa(13); kKir=pa(14); kmK=pa(15); 
tmCa=pa(16); thCa=pa(17); tmK=pa(18); 
mCa0=pa(19); hCa0=pa(20); mK0=pa(21); 
C=pa(22);                     


function y=xinf(VH,V12,k)
    y = 1./(1+exp((V12-VH) ./k))
endfunction

function y=alpha(V,hCa)
    y = gCa.*xinf(V,V12mCa,kmCa).*hCa
endfunction

function y=betta(V)
    y = gKir.*xinf(V,V12hKir,kKir)
endfunction

function y=delta(V)
    y = gK.*xinf(V,V12mK,kmK)
endfunction

function y=fmCa(V)
    y = (1/kmCa)*exp((V12mCa-V) ./kmCa).*xinf(V,V12mCa,kmCa)
endfunction

function y=fhCa(V)
    y = (1/khK)*exp((V12hCa-V) ./khCa).*xinf(V,V12hCa,khCa)
endfunction

function y=fhKir(V)
    y = (1/kKir)*exp((V12hKir-V) ./kKir).*xinf(V,V12hKir,kKir)
endfunction

function y=fmK(V)
    y = (1/kmK)*exp((V12mK-V) ./kmK).*xinf(V,V12mK,kmK)
endfunction

// Definition of the term a

function y=a(V,hCa)
    y = -(1/C)*[alpha(V,hCa).*(1 + fmCa(V).*(V-ECa)) + betta(V).*(1 + fhKir(V).*(V-EK)) + delta(V).*(1 + fmK(V).*(V-EK)) + gL]
endfunction

function y=divF(V,hCa)
    y = a(V,hCa)-1/thCa
endfunction

//V=[-100:0.01:50]
////plot2d(vecV,Iinf(vecV),2)
//
//plot2d(V,divF(V,0),2)
//xlabel("$V$", "fontsize", 5)
//ylabel("$\text{div}F(V,0)$", "fontsize", 5)
//a=gca();
//a.font_size=3;
//xtitle("AIY")
//t=a.title
//t.font_size=5

//plot2d(V,alpha(V),3)

V=[-100:5:50]';
h=[0:0.01:1]';
y=feval(V,h,divF);
plot3d1(V,h,y)
cmap=hotcolormap(64);
divF=gcf();//figure courante
divF.color_map=cmap;


