/////////////////////////////////////////////////////////////
///////////////     Récupération données      ///////////////
/////////////////////////////////////////////////////////////

a = read("/home/naudin/Documents/FichierScilab/EstimationRIM/Fig1ARIMCurrentClampTrace.txt",-1,12);
A=a(2489:14988,2:$)*1000;
t=linspace(0,50,12500);

for i=[1:1:11]
    plot2d(t,A(:,i),3)
end

//////////////////////////////////////////////////////////////
///////////////     Plot Voltage Ca,t+K+L      ///////////////
//////////////////////////////////////////////////////////////

//pa=[   0.1852136
//   0.0065778
//   3.2718946
//   0.2154436
//   26.754021
//  -18.845031
//  -52.533596
//  -24.834292
//  -90.
//  -2.
//  -39.999334
//   10.456785
//  -4.1033404
//   11.236174
//  -1.
//   0.0001
//   0.0001
//   0.0164793
//   0.001
//   0.999
//   0.5652175
//   0.043537]
//   

pa=[   0.6811548
   0.2540603
   1.8120934
   0.0008546
   20.596118
  -38.588103
  -90.
  -2.
  -86.988043
  -9.6325219
  -24.275244
   5.1956752
  -30.
   4.8404004
  -21.843512
   0.5483041
   0.0515691
   0.609105
   0.001
   0.001
   0.1130469
   0.0423273]


function y=xinf(VH,V12,k)
    y=1 ./(1+exp((V12-VH) ./k));
endfunction

function [Hdot]=HH11(t,x,pa)
    Hdot=zeros(4,1);
    Hdot(1)=(1/pa(22))*(-pa(1)*x(2)*(x(1)-pa(5)) - pa(2)*xinf(x(1),pa(9),pa(13))*(x(1)-pa(6)) - pa(3)*x(3)*x(4)*(x(1)-pa(6)) - pa(4)*(x(1)-pa(7)) + I)
    Hdot(2)=(xinf(x(1),pa(8),pa(12))-x(2))/pa(16)
    Hdot(3)=(xinf(x(1),pa(10),pa(14))-x(3))/pa(17)
    Hdot(4)=(xinf(x(1),pa(11),pa(15))-x(4))/pa(18)
endfunction

function [Hdot]=HHred(t,x,pa)
    Hdot(1)=(1/pa(22))*(-pa(1)*xinf(x(1),pa(8),pa(12))*(x(1)-pa(5)) - pa(2)*xinf(x(1),pa(9),pa(13))*(x(1)-pa(6)) - pa(3)*xinf(x(1),pa(10),pa(14))*x(2)*(x(1)-pa(6)) - pa(4)*(x(1)-pa(7)) + I)
    Hdot(2)=(xinf(x(1),pa(11),pa(15))-x(2))/pa(18)
endfunction

stim=[-15:5:35];
t0=0;

//for I=stim
//    condini = [-38; pa(19); pa(20); pa(21)]
//    xred=ode(condini,t0,t,HHred); 
//    xred1=xred(1,:);
//    plot2d(t(1:250),xred1(1:250),2);
////    plot2d(t(1:250),xinf(xred1(1:250),pa(8),pa(12)),2)
////    plot2d(t(1:250),xinf(xred1(1:250),pa(10),pa(14)),2)
//end
//
//for I=stim
//    condini = [-38; pa(19); pa(20); pa(21)]
//    x=ode(condini,t0,t,HH11); 
//    x1=x(1,:);
//    x2=x(2,:);
//    x3=x(3,:);
//    plot2d(t(1:250),x1(1:250),5);
////    plot2d(t(1:250),x2(1:250),5)
////    plot2d(t(1:250),x3(1:250),5)
//end

for I=stim
    condini = [-38; pa(21)]
    xred=ode(condini,t0,t,HHred); 
    xred1=xred(1,:);
    plot2d(t,xred1,2);
//    plot2d(t(1:250),xinf(xred1(1:250),pa(8),pa(12)),2)
//    plot2d(t(1:250),xinf(xred1(1:250),pa(10),pa(14)),2)
end

//for I=stim
//    condini = [-38; pa(19); pa(20); pa(21)]
//    x=ode(condini,t0,t,HH11); 
//    x1=x(1,:);
//    x2=x(2,:);
//    x3=x(3,:);
//    plot2d(t,x1,5);
////    plot2d(t(1:250),x2(1:250),5)
////    plot2d(t(1:250),x3(1:250),5)
//end



xlabel("$t\; (10^{-1}s)$", "fontsize", 5)
ylabel("$V(t)$", "fontsize", 5)
xtitle("$\text{RIM} \mbox{ vs. } \text{reduced model}$")
a=gca();
t=a.title
t.font_size=5
a.font_size=4
//legends(["$t\rightarrow V(t)$";"$t\rightarrow V_{reduced}(t)$"],[5,2],5)


