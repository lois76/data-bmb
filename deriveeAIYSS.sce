pa=[   0.4140729
   0.1251257
   0.1911187
   0.1792776
   61.399448
  -99.777478
  -53.454163
  -2.9436432
  -47.587021
  -89.989312
  -9.6048379
   8.8823942
  -28.299354
  -6.999083
   6.6768077
   0.4509206
   8.5186795
   0.0043276
   0.0220836
   0.9685221
   0.001
   0.0290987]

gCa=pa(1); gKir=pa(2); gK=pa(3); gL=pa(4);
ECa=pa(5); EK=pa(6); EL=pa(7);
V12mCa=pa(8); V12hCa=pa(9); V12hKir=pa(10); V12mK=pa(11); 
kmCa=pa(12); khCa=pa(13); kKir=pa(14); kmK=pa(15); 
tmCa=pa(16); thCa=pa(17); tmK=pa(18); 
mCa0=pa(19); mK0=pa(20); hK0=pa(21);
C=pa(22);

function y=xinf(VH,V12,k)
    y = 1./(1+exp((V12-VH) ./k))
endfunction

function y=alpha(V)
    y = gCa.*xinf(V,V12mCa,kmCa).*xinf(V,V12hCa,khCa)
endfunction

function y=betta(V)
    y = gKir.*xinf(V,V12hKir,kKir)
endfunction

function y=delta(V)
    y = gK.*xinf(V,V12mK,kmK)
endfunction

function y=fmCa(V)
    y = (1/kmCa)*exp((V12mCa-V) ./kmCa).*xinf(V,V12mCa,kmCa)
endfunction

function y=fhCa(V)
    y = (1/khCa)*exp((V12hCa-V) ./khCa).*xinf(V,V12hCa,khCa)
endfunction

function y=fhKir(V)
    y = (1/kKir)*exp((V12hKir-V) ./kKir).*xinf(V,V12hKir,kKir)
endfunction

function y=fmK(V)
    y = (1/kmK)*exp((V12mK-V) ./kmK).*xinf(V,V12mK,kmK)
endfunction

function y=IinfDer(V)
    y = alpha(V).*[1+(fmCa(V)+fhCa(V)).*(V-ECa)] + betta(V).*[1+fhKir(V).*(V-EK)] + delta(V).*[1+fmK(V).*(V-EK)] + gL
endfunction




//function y=Iinf(V)
//    y = pa(1).*xinf(V,pa(8),pa(12)).*(V-pa(5)) + pa(2).*xinf(V,pa(9),pa(13)).*(V-pa(6)) + pa(3).*xinf(V,pa(10),pa(14)).*xinf(V,pa(11),pa(15)).*(V-pa(6)) + pa(4).*(V-pa(7))
//endfunction

V=[-100:0.01:50]
//plot2d(vecV,Iinf(vecV),2)

//plot2d(vecV,fmCa(vecV),4)
//plot2d(vecV,fhKir(vecV),3)
//plot2d(vecV,fmK(vecV),2)
//plot2d(vecV,fhK(vecV),3)

plot2d(V,IinfDer(V),2)
xlabel("$V$", "fontsize", 5)
ylabel("$I_{\infty}(V)$", "fontsize", 5)
a=gca();
a.font_size=4;
xtitle("AIY")
t=a.title
t.font_size=5

//plot2d(V,alpha(V),3)
//
//mu=[0:0.01:10]
//plot2d(mu,sqrt(mu))
//plot2d(mu,-sqrt(mu))
//xlabel("$\mu$", "fontsize", 5)
//ylabel("$\eta$", "fontsize", 5)
//a=gca();
//a.font_size=4;
////xtitle("AIY")
//t=a.title
//t.font_size=5


xlabel("$f(V)$", "fontsize", 5)




