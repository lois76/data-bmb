//Divergence of Ca,p+Kir+K,t+L-model for the RIM neuron

pa=[0.1852136 0.0065778 3.2718946 0.2154436 26.754021 -18.845031 -52.533596 -24.834292 -90. -2. -39.999334 10.456785 -4.1033404 11.236174 -1. 0.0001 0.0001 0.0164793 0.001 0.999 0.5652175 0.043537]

function y=xinf(VH,V12,k)
    y=1 ./(1+exp((V12-VH) ./k));
endfunction

function [Hdot]=HHred(t,x,pa)
    Hdot(1)=(1/pa(22))*(-pa(1)*xinf(x(1),pa(8),pa(12))*(x(1)-pa(5)) - pa(2)*xinf(x(1),pa(9),pa(13))*(x(1)-pa(6)) - pa(3)*xinf(x(1),pa(10),pa(14))*x(2)*(x(1)-pa(6)) - pa(4)*(x(1)-pa(7)) + I)
    Hdot(2)=(xinf(x(1),pa(11),pa(15))-x(2))/pa(18)
endfunction

function y = div(V,hK)
    y = - pa(1) ./(1+exp((pa(8)-V) ./pa(12))) - pa(1).*(V-pa(5)).*exp((pa(8)-V) ./pa(12))./(pa(12).*(1+exp((pa(8)-V) ./pa(12)))^2) - pa(2)./(1+exp((pa(9)-V) ./pa(13))) - pa(2).*(V-pa(6)).*exp((pa(9)-V) ./pa(13))./(pa(13).*(1+exp((pa(9)-V) ./pa(13)))^2) - pa(3).*hK./(1+exp((pa(11)-V) ./pa(15))) - pa(3).*hK.*(V-pa(6)).*exp((pa(10)-V) ./pa(14))./(pa(14).*(1+exp((pa(10)-V) ./pa(14)))^2) - pa(4) - 1/pa(18)
endfunction

//V=[-100:1:50]';
//h=[0:0.05:1]';
//y=feval(V,h,div);
//plot3d1(V,h,y)
//cmap=hotcolormap(64);
//mort=gcf();//figure courante
//mort.color_map=cmap;

vecV=[-300:0.01:200]
plot2d(vecV,div(vecV,0),2)

//V=[-150:1:150]';
//h=[0:1:1]';
//y=feval(V,h,div);
//plot3d1(V,h,y)

xlabel("$V\;h_K\;h_{Ca}\;\text{div}F(V,h_{Ca})\;\text{div}F(V,h_{K})$")
