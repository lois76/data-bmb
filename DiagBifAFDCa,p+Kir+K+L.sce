
/////////////////////////////////////////////////////////////
///////////////     Récupération données      ///////////////
/////////////////////////////////////////////////////////////

// Paramètres avec taux d'activation en ds
pa=[   2.9834075
   2.3726436
   7.3624916
   0.0001
   20.
  -79.737466
  -90.
  -2.
  -85.742554
  -2.8337565
  -46.55807
   8.6755918
  -8.9239096
   9.9984561
  -30.
   12.961377
   0.0376808
   3.714941
   0.001
   0.001
   0.5948121
   0.0579676
]

//Définition d'une fonction qui renvoiel'élément avec la partie réelle maximale d'un vecteur

function y=maxVec(u)
    maxMember=real(u(1))
    for i=2:length(u)
        if real(u(i))>maxMember then
            maxMember=real(u(i))
        end
    end
    y=maxMember
endfunction

///////////////////////////////////////////////////////////
/////    Calcul des points d'équilibres du système    /////
///////////////////////////////////////////////////////////

function y=xinf(VH,V12,k)
    y=1 ./(1+exp((V12-VH) ./k));
endfunction

function y=Iinf(V)
    y = pa(1).*xinf(V,pa(8),pa(12)).*(V-pa(5)) + pa(2).*xinf(V,pa(9),pa(13)).*(V-pa(6)) + pa(3).*xinf(V,pa(10),pa(14)).*xinf(V,pa(11),pa(15)).*(V-pa(6)) + pa(4).*(V-pa(7))
endfunction

function y=Vequi(V)
    y=Iinf(V)-I
endfunction

//Veq=fsolve(-70,Vequi)
//
//vecV=[-110:0.0001:100];
//plot(vecV,Iinf(vecV),2);

//xlabel("$V$", "fontsize", 3)
//ylabel("$I_{\infty}(V)$", "fontsize", 3)

//////////////////////////////////////////////////////////
/////    Détermination des seuils de bifurcations    /////
//////////////////////////////////////////////////////////

V1=[-72:0.000001:-67];
i1=Iinf(V1);
s1=max(i1);
v1=V1(find(max(i1)==i1))

V2=[-49:0.000001:-42];
i2=Iinf(V2);
s2=min(i2);
v2=V2(find(min(i2)==i2))



//////////////////////////////////////////////////
/////    Écriture du système Ca,p+Kir+K+L    /////
//////////////////////////////////////////////////

function y=G(V,mCa,mK,hK)
    y=(1/pa(22))*(pa(1)*mCa*(V-pa(5)) + pa(2)*xinf(V,pa(9),pa(13))*(V-pa(6)) + pa(3)*mK*hK*(V-pa(6)) + pa(4)*(V-pa(7)))
endfunction

function y=f(z,I)
    f1=-G(z(1),z(2),z(3),z(4))+I
    f2=(xinf(z(1),pa(8),pa(12))-z(2))/pa(16)
    f3=(xinf(z(1),pa(10),pa(14))-z(3))/pa(17)
    f4=(xinf(z(1),pa(11),pa(15))-z(4))/pa(18)
    y=[f1;f2;f3;f4]
endfunction

///////////////////////////////////////
///  Calcul équilibre + stabilité  ////
///////////////////////////////////////

/////////////////////////////////////////////////
///  Pour I<s2 --> unique point d'équilibre  ////
/////////////////////////////////////////////////

for I=[-15:0.1:s2-0.1]
    Veq=fsolve(-90,Vequi)
    mCaeq=xinf(Veq,pa(8),pa(12));
    mKeq=xinf(Veq,pa(10),pa(14));
    hKeq=xinf(Veq,pa(11),pa(15));
    zeq=[Veq;mCaeq;mKeq;hKeq];
    J=numderivative(list(f, I), zeq); // matrice jacobienne évaluée au pt d'équilibre zeq
    lambda=spec(J);//valeur propre
    if maxVec(lambda)<0 then
        plot(I,Veq,'b.')
    else plot(I,Veq,'r.')
    end
end

/////////////////////////////////////////////////
///  Pour I=s2 --> deux points d'équilibres  ////
/////////////////////////////////////////////////

I=s2

//calcul pour le premier point d'équilibre 
Veq1=fsolve(-90,Vequi)
Veq2=fsolve(-33,Vequi)

plot(I,Veq1,'b.')
plot(I,Veq2,'g.')//saddle-node

//////////////////////////////////////////////////////
////  Pour s2<I<s1 --> trois points d'équilibres  ////
//////////////////////////////////////////////////////

for I=[s2+0.01:0.01:s1-0.01]
    //1er pt d'équilibre
    Veq3=fsolve(-90,Vequi)
    mCaeq3=xinf(Veq3,pa(8),pa(12));
    mKeq3=xinf(Veq3,pa(10),pa(14));
    hKeq3=xinf(Veq3,pa(11),pa(15));
    zeq3=[Veq3;mCaeq3;mKeq3;hKeq3];
    J=numderivative(list(f, I), zeq3); // matrice jacobienne évaluée au pt d'équilibre zeq
    lambda=spec(J);//valeur propre
    if maxVec(lambda)<0 then
        plot(I,Veq3,'b.')
    else plot(I,Veq3,'r.')
    end
    //2ème pt d'équilibre
    Veq4=fsolve(-55,Vequi)
    mCaeq4=xinf(Veq4,pa(8),pa(12));
    mKeq4=xinf(Veq4,pa(10),pa(14));
    hKeq4=xinf(Veq4,pa(11),pa(15));
    zeq4=[Veq4;mCaeq4;mKeq4;hKeq4];
    J=numderivative(list(f, I), zeq4); // matrice jacobienne évaluée au pt d'équilibre zeq
    lambda=spec(J);//valeur propre
    if maxVec(lambda)<0 then
        plot(I,Veq4,'b.')
    else plot(I,Veq4,'r.')
    end
    //3ème pt d'équilibre
    Veq5=fsolve(-35,Vequi)
    mCaeq5=xinf(Veq5,pa(8),pa(12));
    mKeq5=xinf(Veq5,pa(10),pa(14));
    hKeq5=xinf(Veq5,pa(11),pa(15));
    zeq5=[Veq5;mCaeq5;mKeq5;hKeq5];
    J=numderivative(list(f, I), zeq5); // matrice jacobienne évaluée au pt d'équilibre zeq
    lambda=spec(J);//valeur propre
    if maxVec(lambda)<0 then
        plot(I,Veq5,'b.')
    else plot(I,Veq5,'r.')
    end
//    plot(I,Veq3,'b.')
//    plot(I,Veq4,'r.')
//    plot(I,Veq5,'b.')
end

/////////////////////////////////////////////////
///  Pour I=s1 --> deux points d'équilibres  ////
/////////////////////////////////////////////////

I=s1
Veq1=fsolve(-90,Vequi)
Veq2=fsolve(-20,Vequi)
plot(I,Veq1,'g.')//saddle-node
plot(I,Veq2,'b.')

/////////////////////////////////////////////////////
////  Pour s1<I --> unique point d'équilibre  ////
/////////////////////////////////////////////////////

for I=[s1+0.1:0.1:35]
    Veq=fsolve(-27,Vequi);
    mCaeq=xinf(Veq,pa(8),pa(12));
    mKeq=xinf(Veq,pa(10),pa(14));
    hKeq=xinf(Veq,pa(11),pa(15));
    zeq=[Veq;mCaeq;mKeq;hKeq];
    J=numderivative(list(f, I), zeq); // matrice jacobienne évaluée au pt d'équilibre zeq
    lambda=spec(J);//valeur propre
    if maxVec(lambda)<0 then
        plot(I,Veq,'b.')
    else plot(I,Veq,'r.')
    end
end

xlabel("$I\; \text{(pA)}$", "fontsize",5)
ylabel("$V_*\; \text{(mV)}$", "fontsize",5)

b=gca();
b.font_size=4

//xlabel("$I$", "fontsize",5)




