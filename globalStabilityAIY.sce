pa=[   0.4140729
   0.1251257
   0.1911187
   0.1792776
   61.399448
  -99.777478
  -53.454163
  -2.9436432
  -47.587021
  -89.989312
  -9.6048379
   8.8823942
  -28.299354
  -6.999083
   6.6768077
   0.4509206
   8.5186795
   0.0043276
   0.0220836
   0.9685221
   0.001
   0.0290987]

gCa=pa(1); gKir=pa(2); gK=pa(3); gL=pa(4);
ECa=pa(5); EK=pa(6); EL=pa(7);
V12mCa=pa(8); V12hCa=pa(9); V12hKir=pa(10); V12mK=pa(11); 
kmCa=pa(12); khCa=pa(13); kKir=pa(14); kmK=pa(15); 
tmCa=pa(16); thCa=pa(17); tmK=pa(18); 
mCa0=pa(19); hCa0=pa(20); mK0=pa(21); 
C=pa(22);

function y=xinf(VH,V12,k)
    y = 1./(1+exp((V12-VH) ./k))
endfunction

// Determination of the unique equilibrium point

function y=Iinf(V)
    y = pa(1).*xinf(V,pa(8),pa(12)).*xinf(V,pa(9),pa(13)).*(V-pa(5)) + pa(2).*xinf(V,pa(10),pa(14)).*(V-pa(6)) + pa(3).*xinf(V,pa(11),pa(15)).*(V-pa(6)) + pa(4).*(V-pa(7))
endfunction

function y=Vequi(V)
    y=Iinf(V)-I
endfunction

I=5
Veq=fsolve(0,Vequi)
heq=xinf(Veq,V12hCa,khCa)

//Plot of the equilibrium point in the phase space
plot(Veq,heq,'r.')

//Plot of model trajectories for different initial conditions
function [Hdot]=HHred(t,x,pa)
    Hdot(1)=(1/pa(22))*(-pa(1)*xinf(x(1),pa(8),pa(12))*x(2)*(x(1)-pa(5)) - pa(2)*xinf(x(1),pa(10),pa(14))*(x(1)-pa(6)) - pa(3)*xinf(x(1),pa(11),pa(15))*(x(1)-pa(6)) - pa(4)*(x(1)-pa(7)) + I)
    Hdot(2)=(xinf(x(1),pa(9),pa(13))-x(2))/pa(17)
endfunction

//stim=[-15:5:35];
//t0=0;
//for I=stim
//    condini = [-53; hCa0]
//    x=ode(condini,t0,t,HHred); 
//    x1=x(1,:);
//    plot2d(t,x1,2);
//end

I=5
u=[0.1 0.2 0.3 0.43 0.55 0.67 0.8]
for i=u
    pa(20)=i;
    condini = [10; pa(20)]
    t=linspace(0,50,12500);
    t0=0;
    x=ode(condini,t0,t,HHred); 
    x1=x(1,:);
    x2=x(2,:);
    plot2d(x1,x2,2);
end


xlabel("$V$", "fontsize", 5)
ylabel("$h_{Ca}$", "fontsize", 5)
a=gca();
a.font_size=4;
xtitle("AIY reduced model")
t=a.title
t.font_size=5
set(gca(),"data_bounds",matrix([-60,10,0,0.9],2,-1)); 
