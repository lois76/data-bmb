/////////////////////////////////////////////////////////////
///////////////     Récupération données      ///////////////
/////////////////////////////////////////////////////////////

a = read("/home/naudin/Documents/FichierScilab/Fourre tout/Fig1A_AIYCurrentClampTrace2.txt",-1,12);
A=a(2489:14988,2:$)*1000;
t=linspace(0,50,12500);

scf();

for i=[1:1:11]
    plot2d(t,A(:,i),3)
end


//////////////////////////////////////////////////////////////
///////////////     Plot Voltage Ca,t+Kir+K+L      ///////////////
//////////////////////////////////////////////////////////////

pa=[0.4140729
   0.1251257
   0.1911187
   0.1792776
   61.399448
  -99.777478
  -53.454163
  -2.9436432
  -47.587021
  -89.989312
  -9.6048379
   8.8823942
  -28.299354
  -6.999083
   6.6768077
   0.4509206
   8.5186795
   0.0043276
   0.0220836
   0.9685221
   0.001
   0.0290987]
   


function y=xinf(VH,V12,k)
    y=1 ./(1+exp((V12-VH) ./k));
endfunction

function [Hdot]=HH(t,x,pa)
    Hdot=zeros(4,1);
    Hdot(1)=(1/pa(22))*(-pa(1)*x(2)*x(3)*(x(1)-pa(5)) - pa(2)*xinf(x(1),pa(10),pa(14))*(x(1)-pa(6)) - pa(3)*x(4)*(x(1)-pa(6)) - pa(4)*(x(1)-pa(7)) + I)
    Hdot(2)=(xinf(x(1),pa(8),pa(12))-x(2))/pa(16)
    Hdot(3)=(xinf(x(1),pa(9),pa(13))-x(3))/pa(17)
    Hdot(4)=(xinf(x(1),pa(11),pa(15))-x(4))/pa(18)
endfunction

function [Hdot]=HHred(t,x,pa)
    Hdot(1)=(1/pa(22))*(-pa(1)*xinf(x(1),pa(8),pa(12))*x(2)*(x(1)-pa(5)) - pa(2)*xinf(x(1),pa(10),pa(14))*(x(1)-pa(6)) - pa(3)*xinf(x(1),pa(11),pa(15))*(x(1)-pa(6)) - pa(4)*(x(1)-pa(7)) + I)
    Hdot(2)=(xinf(x(1),pa(9),pa(13))-x(2))/pa(17)
endfunction

stim=[-15:5:35];
t0=0;

//for I=stim
//    condini = [-38; pa(19); pa(20); pa(21)]
//    xred=ode(condini,t0,t,HHred); 
//    xred1=xred(1,:);
//    plot2d(t(1:250),xred1(1:250),2);
////    plot2d(t(1:250),xinf(xred1(1:250),pa(8),pa(12)),2)
////    plot2d(t(1:250),xinf(xred1(1:250),pa(10),pa(14)),2)
//end
//
//for I=stim
//    condini = [-53; pa(19); pa(20); pa(21)]
//    x=ode(condini,t0,t,HH); 
//    x1=x(1,:);
//    x2=x(2,:);
//    x3=x(3,:);
//    plot2d(t,x1,5);
////    plot2d(t(1:250),x2(1:250),5)
////    plot2d(t(1:250),x3(1:250),5)
//end

for I=stim
    condini = [-53; pa(20)]
    xred=ode(condini,t0,t,HHred); 
    xred1=xred(1,:);
    plot2d(t,xred1,2);
//    plot2d(t(1:250),xinf(xred1(1:250),pa(8),pa(12)),2)
//    plot2d(t(1:250),xinf(xred1(1:250),pa(10),pa(14)),2)
end

//for I=stim
//    condini = [-38; pa(19); pa(20); pa(21)]
//    x=ode(condini,t0,t,HH11); 
//    x1=x(1,:);
//    x2=x(2,:);
//    x3=x(3,:);
//    plot2d(t,x1,5);
////    plot2d(t(1:250),x2(1:250),5)
////    plot2d(t(1:250),x3(1:250),5)
//end



xlabel("$t\; (10^{-1}s)$", "fontsize", 5)
ylabel("$V(t)$", "fontsize", 5)
xtitle("$\text{AIY} \mbox{ vs. } \text{reduced model}$")
a=gca();
t=a.title
t.font_size=5
a.font_size=4
//legends(["$t\rightarrow V(t)$";"$t\rightarrow V_{reduced}(t)$"],[5,2],5)


