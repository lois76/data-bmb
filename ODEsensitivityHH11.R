library(ODEsensitivity)


# Dans la définition du modèle HH, comment faire avec le I ? Le fixer ? 
# Est-ce que je dois considérer les conditions initiales comme des paramètres dont il faudrait étudier l'influence ?

# On définit notre modèle Hodgkin-Huxley (HH)  
HHmod <- function(Time, State, Pars) {
	with(as.list(c(State, Pars)), {
		#ICa <- gCa*x1*x2*(V-ECa)
		#IK <- gK*x3*x4*(V-EK)
		#IL <- gL*(V-EL)
		I <- 15
		dV <- (1/C)*(- gCa*mCa*(V-ECa) - gKir*(1/(1+exp((V12hKir-V)/khKir)))*(V-EK) - gK*mK*hK*(V-EK) - gL*(V-EL) + I)
		dmCa <- (1/(1+exp((V12mCa-V)/kmCa))-mCa)/tmCa
		dmK <- (1/(1+exp((V12mK-V)/kmK))-mK)/tmK
		dhK <- (1/(1+exp((V12hK-V)/khK))-hK)/thK

		return(list(c(dV, dmCa, dmK, dhK)))
	})
}

# Dans la définition du modèle HH, comment faire avec le I ? Le fixer ? 

HHpars <- c("gCa", "gKir", "gK", "gL", "ECa", "EK", "EL", "V12mCa", "V12hKir", "V12mK", "V12hK", "kmCa", "khKir", "kmK", "khK", "tmCa", "tmK", "thK", "C") # vecteurs des paramètres à analyser qui sont les paramètres des équations
HHbinf <- c(0.0001, 0.0001, 0.0001, 0.0001, 20, -100, -90, -90, -90, -90, -90, 1, -30, 1, -30, 0.0001, 0.0001, 0.0001, 0.001) # bornes contraintes inférieures pour chacun des params
HHbsup <- c(50, 50, 50, 50, 150, -2, 30, -2, -2, -2, -2, 30, -1, 30, -1, 20, 20, 20, 10) # bornes contraintes supérieures pour chacun des params
HHinit <- c(V = -38, mCa = 0.001, mK = 0.999, hK = 0.5652) # conditions initiales du système
HHtimes <- c(0.001, seq(1, 50, by = 0.25))


HHres_sobol <- ODEsobol(mod = HHmod,
			pars = HHpars,
			state_init = HHinit,
			times = HHtimes,
			n = 5000,
			rfuncs = "runif",
			rargs = paste0("min = ", HHbinf,
					", max = ", HHbsup),
			sobol_method = "Martinez",
			ode_method = "lsoda",
			parallel_eval = TRUE,
			parallel_eval_ncores = parallel::detectCores())

str(HHres_sobol, vec.len = 3, give.attr = FALSE)

pdf(file="/home/naudin/Documents/mathematical-analysis/SensitivityAnalysis/allParamsRIMAFD.pdf")
#plot(HHres_sobol, pars_plot = c("ECa", "EK", "EL", "gCa", "gL", "V12mCa"), state_plot = "V")
#plot(HHres_sobol, pars_plot = c("gCa", "gKir", "gK", "gL","ECa", "EK", "EL"), state_plot = "V")
plot(HHres_sobol, state_plot = "V")
dev.off()

#plot(HHres_sobol)
#plot(HHres_sobol, pars_plot = c("kmCa", "khKir", "kmK", "khK"), state_plot = "V")
#plot(HHres_sobol, pars_plot = c("gCa", "gK", "gL", "ECa", "EK", "EL"), state_plot = "V")
#plot(HHres_sobol, pars_plot = c("V12mCa", "V12khKir", "V12mK", "V12hK", "kmCa", "khKir", "kmK", "khK"), state_plot = "V")


