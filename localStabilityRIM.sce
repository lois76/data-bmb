pa=[   0.1852136
   0.0065778
   3.2718946
   0.2154436
   26.754021
  -18.845031
  -52.533596
  -24.834292
  -90.
  -2.
  -39.999334
   10.456785
  -4.1033404
   11.236174
  -1.
   0.0001
   0.0001
   0.0164793
   0.001
   0.999
   0.5652175
   0.043537]

//pa=[   0.244081
//   0.0001
//   10.481652
//   0.2294535
//   49.064735
//  -36.854555
//  -60.884187
//  -18.534373
//  -7.0034157
//  -7.2680557
//  -41.951392
//   17.240955
//  -10.308111
//   9.3794057
//  -8.219171
//   0.0001
//   0.0001
//   0.0198213
//   0.0425587
//   0.321176
//   0.1507738
//   0.0375561]

gCa=pa(1); gKir=pa(2); gK=pa(3); gL=pa(4);
ECa=pa(5); EK=pa(6); EL=pa(7);
V12mCa=pa(8); V12hKir=pa(9); V12mK=pa(10); V12hK=pa(11);
kmCa=pa(12); kKir=pa(13); kmK=pa(14); khK=pa(15);
tmCa=pa(16); tmK=pa(17); thK=pa(18)
mCa0=pa(19); mK0=pa(20); hK0=pa(21);
C=pa(22);

function y=xinf(VH,V12,k)
    y = 1./(1+exp((V12-VH) ./k))
endfunction

// Determination of the unique equilibrium point

function y=f(x,I)
    f1=(1/pa(22))*(-pa(1)*xinf(x(1),pa(8),pa(12))*(x(1)-pa(5)) - pa(2)*xinf(x(1),pa(9),pa(13))*(x(1)-pa(6)) - pa(3)*xinf(x(1),pa(10),pa(14))*x(2)*(x(1)-pa(6)) - pa(4)*(x(1)-pa(7)) + I)
    f2=(xinf(x(1),pa(11),pa(15))-x(2))/pa(18)
    y=[f1;f2]
endfunction

function y=Iinf(V)
    y = pa(1).*xinf(V,pa(8),pa(12)).*(V-pa(5)) + pa(2).*xinf(V,pa(9),pa(13)).*(V-pa(6)) + pa(3).*xinf(V,pa(10),pa(14)).*xinf(V,pa(11),pa(15)).*(V-pa(6)) + pa(4).*(V-pa(7))
endfunction

function y=Vequi(V)
    y=Iinf(V)-I
endfunction

vecLambda=[];
for I=[-15:0.01:35]
    Veq=fsolve(0,Vequi)
    heq=xinf(Veq,V12hK,khK)
    zeq=[Veq;heq];
    J=numderivative(list(f, I), zeq) // matrice jacobienne évaluée au pt d'équilibre zeq
    lambda=spec(J);//valeurs propres de la jacobienne
    vecLambda=[vecLambda lambda']
//    plot(I,lambda(1),'b.')
//    plot(I,lambda(2),'b.')
//    disp(lambda)
//    disp("==========")
end


//xlabel("$I$", "fontsize", 5)
//ylabel("$\text{Re}(\lambda)$", "fontsize", 5)
//a=gca();
//a.font_size=3;
//xtitle("RIM")
//t=a.title
//t.font_size=4























// Determination of the nature of the stationary point

function y=alpha(V)
    y = gCa.*xinf(V,V12mCa,kmCa)
endfunction

function y=betta(V)
    y = gKir.*xinf(V,V12hKir,kKir)
endfunction

function y=delta(V,hK)
    y = gK.*hK.*xinf(V,V12mK,kmK)
endfunction

function y=fmCa(V)
    y = (1/kmCa)*exp((V12mCa-V) ./kmCa).*xinf(V,V12mCa,kmCa)
endfunction

function y=fhKir(V)
    y = (1/kKir)*exp((V12hKir-V) ./kKir).*xinf(V,V12hKir,kKir)
endfunction

function y=fmK(V)
    y = (1/kmK)*exp((V12mK-V) ./kmK).*xinf(V,V12mK,kmK)
endfunction

function y=fhK(V)
    y = (1/khK)*exp((V12hK-V) ./khK).*xinf(V,V12hK,khK)
endfunction

function y=IinfDer(V)
    y = alpha(V).*fmCa(V).*(V-ECa) + (delta(V).*fmK(V) + delta(V).*fhK(V) + betta(V).*fhKir(V)).*(V-EK) + alpha(V) + betta(V) + delta(V) + gL
endfunction

//Definition term a, b, c and d of the jacobian matrix (see paper)

function y=a(V,hK)
    y = -(1/C)*[alpha(V).*fmCa(V).*(V-ECa) + betta(V).*fhKir(V).*(V-EK) + delta(V,hK).*fmK(V).*(V-EK) + alpha(V) + betta(V) + delta(V,hK) + gL]
endfunction

function y=b(V)
    y = -(gK/C)*xinf(V,V12mK,kmK).*(V-EK)
endfunction

function y=c(V)
    y = xinf(V,V12hK,khK).*fhK(V)
endfunction

V=[-100:0.01:50]

//plot2d(V,IinfDer(V),2)
//xlabel("$V$", "fontsize", 5)
//ylabel("$I_{\infty}(V)$", "fontsize", 5)
//a=gca();
//a.font_size=3;
////xtitle("RIM")
//t=a.title
//t.font_size=4






