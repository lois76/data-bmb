//pa=[   0.1852136
//   0.0065778
//   3.2718946
//   0.2154436
//   26.754021
//  -18.845031
//  -52.533596
//  -24.834292
//  -90.
//  -2.
//  -39.999334
//   10.456785
//  -4.1033404
//   11.236174
//  -1.
//   0.0001
//   0.0001
//   0.0164793
//   0.001
//   0.999
//   0.5652175
//   0.043537]

//pa=[   0.244081
//   0.0001
//   10.481652
//   0.2294535
//   49.064735
//  -36.854555
//  -60.884187
//  -18.534373
//  -7.0034157
//  -7.2680557
//  -41.951392
//   17.240955
//  -10.308111
//   9.3794057
//  -8.219171
//   0.0001
//   0.0001
//   0.0198213
//   0.0425587
//   0.321176
//   0.1507738
//   0.0375561]

pa=[   0.6811548
   0.2540603
   1.8120934
   0.0008546
   20.596118
  -38.588103
  -90.
  -2.
  -86.988043
  -9.6325219
  -24.275244
   5.1956752
  -30.
   4.8404004
  -21.843512
   0.5483041
   0.0515691
   0.609105
   0.001
   0.001
   0.1130469
   0.0423273]


gCa=pa(1); gKir=pa(2); gK=pa(3); gL=pa(4);
ECa=pa(5); EK=pa(6); EL=pa(7);
V12mCa=pa(8); V12hKir=pa(9); V12mK=pa(10); V12hK=pa(11);
kmCa=pa(12); kKir=pa(13); kmK=pa(14); khK=pa(15);
tmCa=pa(16); tmK=pa(17); thK=pa(18)
mCa0=pa(19); mK0=pa(20); hK0=pa(21);
C=pa(22);

function y=xinf(VH,V12,k)
    y = 1./(1+exp((V12-VH) ./k))
endfunction

function y=alpha(V)
    y = gCa.*xinf(V,V12mCa,kmCa)
endfunction

function y=betta(V)
    y = gKir.*xinf(V,V12hKir,kKir)
endfunction

function y=delta(V)
    y = gK.*xinf(V,V12mK,kmK).*xinf(V,V12hK,khK)
endfunction

function y=fmCa(V)
    y = (1/kmCa)*exp((V12mCa-V) ./kmCa).*xinf(V,V12mCa,kmCa)
endfunction

function y=fhKir(V)
    y = (1/kKir)*exp((V12hKir-V) ./kKir).*xinf(V,V12hKir,kKir)
endfunction

function y=fmK(V)
    y = (1/kmK)*exp((V12mK-V) ./kmK).*xinf(V,V12mK,kmK)
endfunction

function y=fhK(V)
    y = (1/khK)*exp((V12hK-V) ./khK).*xinf(V,V12hK,khK)
endfunction

function y=r1(V)
    y = alpha(V).*fmCa(V).*(V-ECa) + (delta(V).*fmK(V) + delta(V).*fhK(V) + betta(V).*fhKir(V)).*(V-EK) + alpha(V) + betta(V) + delta(V) + gL
endfunction

function y=r2(V)
    y = (gCa/kmCa).*(V-ECa) + (gK/kmK + delta(V).*fhK(V) + betta(V).*fhKir(V)).*(V-EK) + alpha(V) + betta(V) + delta(V) + gL
endfunction

function y=f1(V)
    y = alpha(V).*fmCa(V).*(V-ECa)
endfunction

function y=f2(V)
    y = (delta(V).*fmK(V) + gK/khK + gKir/kKir).*(V-EK)
endfunction

function y=f3(V)
    y = alpha(V) + betta(V) + delta(V) + gL
endfunction

function y=IinfDer(V)
    y = alpha(V).*fmCa(V).*(V-ECa) + (delta(V).*fmK(V) + delta(V).*fhK(V) + betta(V).*fhKir(V)).*(V-EK) + alpha(V) + betta(V) + delta(V) + gL
endfunction


function y=Iinf(V)
    y = pa(1).*xinf(V,pa(8),pa(12)).*(V-pa(5)) + pa(2).*xinf(V,pa(9),pa(13)).*(V-pa(6)) + pa(3).*xinf(V,pa(10),pa(14)).*xinf(V,pa(11),pa(15)).*(V-pa(6)) + pa(4).*(V-pa(7))
endfunction

vecV=[-100:0.01:50]
//plot2d(vecV,Iinf(vecV),2)

//plot2d(vecV,fmCa(vecV),4)
//plot2d(vecV,fhKir(vecV),3)
//plot2d(vecV,fmK(vecV),2)
//plot2d(vecV,fhK(vecV),3)

plot2d(vecV,IinfDer(vecV),2)
xlabel("$V$", "fontsize", 5)
ylabel("$I_{\infty}(V)$", "fontsize", 5)
a=gca();
a.font_size=4;
xtitle("RIM")
t=a.title
t.font_size=5





