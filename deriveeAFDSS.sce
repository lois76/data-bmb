pa=[   7.0334139
   2.2178775
   4.4553322
   0.0001
   20.
  -79.528443
  -90.
  -2.
  -84.162844
  -5.6476847
  -2.
   8.0831488
  -8.9239096
   9.9984561
  -24.218668
   5.795099
   0.0376808
   0.0001
   0.001
   0.001
   0.3349279
   0.0579676
]


gCa=pa(1); gKir=pa(2); gK=pa(3); gL=pa(4);
ECa=pa(5); EK=pa(6); EL=pa(7);
V12mCa=pa(8); V12hKir=pa(9); V12mK=pa(10); V12hK=pa(11);
kmCa=pa(12); kKir=pa(13); kmK=pa(14); khK=pa(15);
tmCa=pa(16); tmK=pa(17); thK=pa(18)
mCa0=pa(19); mK0=pa(20); hK0=pa(21);
C=pa(22);

function y=xinf(VH,V12,k)
    y = 1./(1+exp((V12-VH) ./k))
endfunction

function y=alpha(V)
    y = gCa.*xinf(V,V12mCa,kmCa)
endfunction

function y=betta(V)
    y = gKir.*xinf(V,V12hKir,kKir)
endfunction

function y=delta(V)
    y = gK.*xinf(V,V12mK,kmK).*xinf(V,V12hK,khK)
endfunction

function y=fmCa(V)
    y = (1/kmCa)*exp((V12mCa-V) ./kmCa).*xinf(V,V12mCa,kmCa)
endfunction

function y=fhKir(V)
    y = (1/kKir)*exp((V12hKir-V) ./kKir).*xinf(V,V12hKir,kKir)
endfunction

function y=fmK(V)
    y = (1/kmK)*exp((V12mK-V) ./kmK).*xinf(V,V12mK,kmK)
endfunction

function y=fhK(V)
    y = (1/khK)*exp((V12hK-V) ./khK).*xinf(V,V12hK,khK)
endfunction

function y=r1(V)
    y = alpha(V).*fmCa(V).*(V-ECa) + (delta(V).*fmK(V) + delta(V).*fhK(V) + betta(V).*fhKir(V)).*(V-EK) + alpha(V) + betta(V) + delta(V) + gL
endfunction

function y=r2(V)
    y = (gCa/kmCa).*(V-ECa) + (gK/kmK + delta(V).*fhK(V) + betta(V).*fhKir(V)).*(V-EK) + alpha(V) + betta(V) + delta(V) + gL
endfunction

function y=f1(V)
    y = alpha(V).*fmCa(V).*(V-ECa)
endfunction

function y=f2(V)
    y = (delta(V).*fmK(V) + gK/khK + gKir/kKir).*(V-EK)
endfunction

function y=f3(V)
    y = alpha(V) + betta(V) + delta(V) + gL
endfunction

function y=IinfDer(V)
    y = alpha(V).*fmCa(V).*(V-ECa) + (delta(V).*fmK(V) + delta(V).*fhK(V) + betta(V).*fhKir(V)).*(V-EK) + alpha(V) + betta(V) + delta(V) + gL
endfunction


function y=Iinf(V)
    y = pa(1).*xinf(V,pa(8),pa(12)).*(V-pa(5)) + pa(2).*xinf(V,pa(9),pa(13)).*(V-pa(6)) + pa(3).*xinf(V,pa(10),pa(14)).*xinf(V,pa(11),pa(15)).*(V-pa(6)) + pa(4).*(V-pa(7))
endfunction

V=[-100:0.01:50]
//plot2d(vecV,Iinf(vecV),2)

//plot2d(vecV,fmCa(vecV),4)
//plot2d(vecV,fhKir(vecV),3)
//plot2d(vecV,fmK(vecV),2)
//plot2d(vecV,fhK(vecV),3)

plot2d(V,IinfDer(V),2)
xlabel("$V_1\;\;\;\;V_2$", "fontsize", 6)
ylabel("$I_{\infty}(V)$", "fontsize", 6)
a=gca();
a.font_size=5;
//xtitle("RIM")
//t=a.title
//t.font_size=5

//plot2d(V,alpha(V),3)




