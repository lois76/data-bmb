/////////////////////////////////////////////////////////////
///////////////     Récupération données      ///////////////
/////////////////////////////////////////////////////////////

//Voltage
A = read("/home/naudin/Documents/GitHub/ParamEstimationDE/differentialEvolution/scilab-scripts/AFDnewDataSecondRecordingsNumberPointsDividedBy4.txt",-1,11);
//A = read("/home/naudin/Documents/article-2/AFD under Extreme Stimulation/Second Recordings/AFDnewDataSecondRecordingsNumberPointsDividedBy4.txt",-1,11);
t=linspace(0,50,12501);
stim=[-15:5:35];

for i=[1:1:11]
    plot2d(t,A(:,i),3)
end

//////////////////////////////////////////////////////////////
///////////////     Plot Voltage Ca,t+K+L      ///////////////
//////////////////////////////////////////////////////////////

pa=[   2.9834075
   2.3726436
   7.3624916
   0.0001
   20.
  -79.737466
  -90.
  -2.
  -85.742554
  -2.8337565
  -46.55807
   8.6755918
  -8.9239096
   9.9984561
  -30.
   12.961377
   0.0376808
   3.714941
   0.001
   0.001
   0.5948121
   0.0579676
]

function y=xinf(VH,V12,k)
    y=1 ./(1+exp((V12-VH) ./k));
endfunction

function [Hdot]=HH11(t,x,pa)
    Hdot=zeros(4,1);
    Hdot(1)=(1/pa(22))*(-pa(1)*x(2)*(x(1)-pa(5)) - pa(2)*xinf(x(1),pa(9),pa(13))*(x(1)-pa(6)) - pa(3)*x(3)*x(4)*(x(1)-pa(6)) - pa(4)*(x(1)-pa(7)) + I)
    Hdot(2)=(xinf(x(1),pa(8),pa(12))-x(2))/pa(16)
    Hdot(3)=(xinf(x(1),pa(10),pa(14))-x(3))/pa(17)
    Hdot(4)=(xinf(x(1),pa(11),pa(15))-x(4))/pa(18)
endfunction

function [Hdot]=HHred(t,x,pa)
    Hdot(1)=(1/pa(22))*(-pa(1)*x(2)*(x(1)-pa(5)) - pa(2)*xinf(x(1),pa(9),pa(13))*(x(1)-pa(6)) - pa(3)*xinf(x(1),pa(10),pa(14))*x(3)*(x(1)-pa(6)) - pa(4)*(x(1)-pa(7)) + I)
    Hdot(2)=(xinf(x(1),pa(8),pa(12))-x(2))/pa(16)
    Hdot(3)=(xinf(x(1),pa(11),pa(15))-x(3))/pa(18)
endfunction

stim=[-15:5:35];
t0=0;

//for I=stim
//    condini = [-38; pa(19); pa(20); pa(21)]
//    xred=ode(condini,t0,t,HHred); 
//    xred1=xred(1,:);
//    plot2d(t(1:250),xred1(1:250),2);
////    plot2d(t(1:250),xinf(xred1(1:250),pa(8),pa(12)),2)
////    plot2d(t(1:250),xinf(xred1(1:250),pa(10),pa(14)),2)
//end
//
//for I=stim
//    condini = [-38; pa(19); pa(20); pa(21)]
//    x=ode(condini,t0,t,HH11); 
//    x1=x(1,:);
//    x2=x(2,:);
//    x3=x(3,:);
//    plot2d(t(1:250),x1(1:250),5);
////    plot2d(t(1:250),x2(1:250),5)
////    plot2d(t(1:250),x3(1:250),5)
//end

for I=stim
    condini = [-76; pa(19); pa(21)]
    xred=ode(condini,t0,t,HHred); 
    xred1=xred(1,:);
    plot2d(t,xred1,2);
//    plot2d(t(1:250),xinf(xred1(1:250),pa(8),pa(12)),2)
//    plot2d(t(1:250),xinf(xred1(1:250),pa(10),pa(14)),2)
end

//for I=stim
//    condini = [-38; pa(19); pa(20); pa(21)]
//    x=ode(condini,t0,t,HH11); 
//    x1=x(1,:);
//    x2=x(2,:);
//    x3=x(3,:);
//    plot2d(t,x1,5);
////    plot2d(t(1:250),x2(1:250),5)
////    plot2d(t(1:250),x3(1:250),5)
//end



xlabel("$t\; (10^{-1}s)$", "fontsize", 5)
ylabel("$V(t)$", "fontsize", 5)
xtitle("$\text{AFD} \mbox{ vs. } \text{reduced model}$")
a=gca();
t=a.title
t.font_size=5
a.font_size=4
//legends(["$t\rightarrow V(t)$";"$t\rightarrow V_{reduced}(t)$"],[5,2],5)


