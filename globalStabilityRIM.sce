pa=[   0.6811548
   0.2540603
   1.8120934
   0.0008546
   20.596118
  -38.588103
  -90.
  -2.
  -86.988043
  -9.6325219
  -24.275244
   5.1956752
  -30.
   4.8404004
  -21.843512
   0.5483041
   0.0515691
   0.609105
   0.001
   0.001
   0.1130469
   0.0423273]
   
gCa=pa(1); gKir=pa(2); gK=pa(3); gL=pa(4);
ECa=pa(5); EK=pa(6); EL=pa(7);
V12mCa=pa(8); V12hKir=pa(9); V12mK=pa(10); V12hK=pa(11);
kmCa=pa(12); kKir=pa(13); kmK=pa(14); khK=pa(15);
tmCa=pa(16); tmK=pa(17); thK=pa(18)
mCa0=pa(19); mK0=pa(20); hK0=pa(21);
C=pa(22);

function y=xinf(VH,V12,k)
    y = 1./(1+exp((V12-VH) ./k))
endfunction

// Determination of the unique equilibrium point

function y=Iinf(V)
    y = pa(1).*xinf(V,pa(8),pa(12)).*(V-pa(5)) + pa(2).*xinf(V,pa(9),pa(13)).*(V-pa(6)) + pa(3).*xinf(V,pa(10),pa(14)).*xinf(V,pa(11),pa(15)).*(V-pa(6)) + pa(4).*(V-pa(7))
endfunction

function y=Vequi(V)
    y=Iinf(V)-I
endfunction

I=-1
Veq=fsolve(0,Vequi)
heq=xinf(Veq,V12hK,khK)

//Plot of the equilibrium point in the phase space
plot(Veq,heq,'r.')

//Plot of model trajectories for different initial conditions
function [Hdot]=HHred(t,x,pa)
    Hdot(1)=(1/pa(22))*(-pa(1)*xinf(x(1),pa(8),pa(12))*(x(1)-pa(5)) - pa(2)*xinf(x(1),pa(9),pa(13))*(x(1)-pa(6)) - pa(3)*xinf(x(1),pa(10),pa(14))*x(2)*(x(1)-pa(6)) - pa(4)*(x(1)-pa(7)) + I)
    Hdot(2)=(xinf(x(1),pa(11),pa(15))-x(2))/pa(18)
endfunction


//stim=[-15:5:35];
//t0=0;
//for I=stim
//    condini = [-38; pa(21)]
//    x=ode(condini,t0,t,HHred); 
//    x1=x(1,:);
//    plot2d(t,x1,2);
//end
//
//I=15

pa(21)=0.9;
condini = [-20; pa(21)]
t=linspace(0,50,12500);
x=ode(condini,t0,t,HHred); 
x1=x(1,:);
x2=x(2,:);
plot2d(x1,x2,2);

//u=[0.1 0.2 0.3 0.43 0.55 0.67 0.8]
//for i=u
//    pa(21)=i;
//    condini = [-50; pa(21)]
//    t=linspace(0,50,12500);
//    t0=0;
//    x=ode(condini,t0,t,HHred); 
//    x1=x(1,:);
//    x2=x(2,:);
//    plot2d(x1,x2,2);
//end





//V=[-100:0.01:50]
//plot2d(V,IinfDer(V),2)
xlabel("$V$", "fontsize", 5)
ylabel("$h_{K}$", "fontsize", 5)
a=gca();
a.font_size=4;
xtitle("RIM reducel model")
t=a.title
t.font_size=5
set(gca(),"data_bounds",matrix([-80,-10,0,1],2,-1)); 




